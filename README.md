


First-time Setup
----------------

Load `env.sh` via `source env.sh`. 
After that, call `NPROC=8 ./install.sh`.
Then, call `env.sh` again in order to activate Python paths that might not have been present before.

If loading the environment for the first time, `env.sh` will consider the environment variable `CUSTOM_PIC_PROFILE`, otherwise a profile from `profiles/` is used.
After that, the profile is sym-linked and can no longer be changed to avoid compatibility issues with installed software in this environment.

Loading the environment after the first setup
---------------------------------------------

Load `env.sh` via `source.env.sh` for activating the environment.
Warning: `env.sh` sets `PIC_PROFILE` to itself.



Using custom software profile files
-----------------------------------


In order to use a custom profile script, follow either of these approaches:

1. Clone this Git repository to a custom place and follow the instructions in "First-time Setup".
2. Load `env.sh` from your own profile. Warnings:
    1. `env.sh` will change the loaded modules.
    2. `env.sh` will set `PIC_PROFILE` to itself. You might do something like `BACKUP_PIC_PROFILE="$PIC_PROFILE"; source env.sh; export PIC_PROFILE="$BACKUP_PIC_PROFILE"`.
