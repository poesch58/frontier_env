#!/usr/bin/env bash

pushd "$(dirname $BASH_SOURCE)" > /dev/null

if [[ -f _env_pre.sh ]]; then
    source _env_pre.sh
fi

__PIC_PROFILE="$(pwd)/"$(basename $BASH_SOURCE)

link_to_profile="$(pwd)/use_this.profile"
if [[ ! -L "$link_to_profile" ]]; then
    if [[ -z "${CUSTOM_PIC_PROFILE+x}"  ]]; then
        ln -s profiles/frontier_hipcc.profile "$link_to_profile"
    else
        ln -s "${CUSTOM_PIC_PROFILE}" "$link_to_profile"
    fi
elif [[ -n "${CUSTOM_PIC_PROFILE+x}" ]]; then
    echo "[Warning] Environment variable CUSTOM_PIC_PROFILE only considered upon calling this environment script for the first time. After that, the profile is stored in '$link_to_profile'." >&2
fi
. "$link_to_profile"

export PREFIX="$(pwd)/local"
export WORKDIR="$(pwd)/build"
export LD_LIBRARY_PATH="$PREFIX/lib:$PREFIX/lib64:$LD_LIBRARY_PATH"
export CMAKE_PREFIX_PATH="$PREFIX:$CMAKE_PREFIX_PATH"
export PIC_PROFILE="$__PIC_PROFILE"
export PATH="$PREFIX/bin:$PATH"
for python_version in "$PREFIX/"lib{64,}/python*; do
    if [[ ! -d "$python_version" ]]; then
        continue
    fi
    export PYTHONPATH="$python_version/site-packages:$PYTHONPATH"
done

if [[ -f _env_post.sh ]]; then
    source _env_post.sh
fi

popd