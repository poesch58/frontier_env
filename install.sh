#!/bin/bash
# call with environment variables WORKDIR PREFIX

install_libfabric() {
    cd "$WORKDIR"
    local version=1.21.0
    if [ -d libfabric-$version ]; then
        return
    fi
    wget -nc https://github.com/ofiwg/libfabric/releases/download/v$version/libfabric-$version.tar.bz2
    tar -xjf libfabric-$version.tar.bz2
    cd libfabric-$version
    cp -r /usr/include/uapi .
    cat << EOF > ./cxi_prov_hw.h
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#define __user
EOF
    cp cxi_prov_hw.h uapi/misc/cxi.h
    cat /usr/include/cxi_prov_hw.h >> ./cxi_prov_hw.h
    cat << EOF >> ./cxi_prov_hw.h
static inline bool cxi_cq_empty(struct cxi_cq * cq){ return cxi_cq_get_cqn(cq) == 0; }
EOF
    cat /usr/include/uapi/misc/cxi.h >> ./uapi/misc/cxi.h
    CFLAGS="-I $(pwd)" ./configure --prefix="$PREFIX"
    CFLAGS="-I $(pwd)" make -j $NPROC
    make install
    cd ..
}

install_ucx() {
    cd "$WORKDIR"
    local version=1.17.0
    if [ -d ucx-$version ]; then
        return
    fi
    wget -nc https://github.com/openucx/ucx/releases/download/v$version/ucx-$version.tar.gz
    tar -xzf ucx-$version.tar.gz
    cd ucx-$version
    CC=gcc CXX=g++ ./configure --prefix="$PREFIX"
    make -j "$NPROC"
    make install
    cd ..
}


install_adios2() {
    # install_bzip2
    cd "$WORKDIR"
    if [ ! -d ADIOS2 ]; then
        git clone -b shm https://github.com/franzpoeschel/ADIOS2
        sed -i 's|if (ADIOS2_HAVE_MPI_CLIENT_SERVER)|if (TRUE)|' ADIOS2/cmake/DetectOptions.cmake
    fi
    local build_python_bindings=ON
    if [ ! -d ADIOS2/build ]; then
        cmake ADIOS2 -B ADIOS2/build -DADIOS2_BUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF \
            -DCMAKE_INSTALL_PREFIX="$PREFIX" -DADIOS2_USE_Fortran=OFF \
            -DADIOS2_USE_BZip2=AUTO -DADIOS2_USE_PNG=OFF \
            -DADIOS2_USE_Python=OFF -DCMAKE_BUILD_TYPE=Release
    fi
    cmake --build ADIOS2/build --target install --parallel $NPROC
}

install_openPMD() {
    cd "$WORKDIR"
    if [ ! -d openPMD-api ]; then
        git clone -b pic_env https://github.com/franzpoeschel/openPMD-api
    fi
    if [ ! -d openPMD-api/build ]; then
        cmake openPMD-api -B openPMD-api/build \
            -DBUILD_EXAMPLES=ON -DBUILD_TESTING=OFF \
            -DopenPMD_USE_PYTHON=ON \
            -DPython_EXECUTABLE="$(which python)" \
            -DCMAKE_INSTALL_PREFIX="$PREFIX"
    fi
    cmake --build openPMD-api/build --target install --parallel $NPROC
    cp openPMD-api/build/bin/10_streaming_read openPMD-api/build/bin/10_streaming_write "$PREFIX/bin/"
}

install_PIConGPU() {
    cd "$WORKDIR"
    if [ ! -d picongpu ]; then
        git clone -b pic_env https://github.com/franzpoeschel/picongpu
    fi
    PATH="$(pwd)/picongpu/bin:$PATH"
    if [ ! -d pic_build ]; then
        pic-create \
            picongpu/share/picongpu/examples/KelvinHelmholtz \
            pic_build
    fi
    cd pic_build
    MAKEFLAGS="-j$NPROC" pic-build -c "-DPIC_OPENPMD_TIMETRACE_NUMBER_OF_FILES=20"
    mkdir -p "$PREFIX/bin"
    # rsync -O --no-perms -avuP bin/* "$PREFIX/bin/"
    cd ..
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    set -x

    if [[ -z "$NPROC" ]]; then
        export NPROC=$(nproc)
    fi

    if [ ! -d "$WORKDIR" ]; then
        mkdir "$WORKDIR"
    fi

    cd "$WORKDIR"


    set +x
    source "$PIC_PROFILE"
    set -x

    MPICC="mpicc -shared" pip install --prefix="$PREFIX" --no-cache-dir --ignore-installed --no-binary=mpi4py mpi4py numpy
    # pip install --ignore-installed --prefix="$PREFIX" wandb nflows geomloss freia chardet pykeops
    # pip install --no-cache-dir --ignore-installed --prefix="$PREFIX" torch torchvision torchaudio --index-url https://download.pytorch.org/whl/rocm5.4.2

    # install_libfabric
    install_adios2
    install_openPMD
    # install_PIConGPU
    echo -e "–––––\nPLEASE RELOAD ./env.sh AFTER FIRST TIME CALLING THIS SCRIPT\n–––––"
fi
