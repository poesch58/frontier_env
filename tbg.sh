#!/bin/bash

# associative arguments per environment:
# TBGSH_SUFFIX: suffix instead of _v1, _v2, ...

set -x
if [[ $# < 2 ]]; then
    echo "Usage: tbg.sh <config> <template>+"
    exit 1
fi
originaldir="$(pwd)"
__f() {
    pushd "$originaldir" > /dev/null
    realpath "$arg"
    popd > /dev/null
}
arg="$1"
config="$(__f)"
shift

if [[ ! -d ./scratch ]]; then
    echo "$(pwd)/scratch must exist and must be a (symlink to a) directory."
    exit 1
fi
basefolder="$(pwd)/scratch/$(date '+%Y-%m-%d')"
mkdir -p "$basefolder"
targetfolder="$basefolder/${config##*/}"
targetfolder="${targetfolder%.cfg}"

if [[ -n "$TBGSH_SUFFIX" ]]; then
    targetfolder="${targetfolder}_${TBGSH_SUFFIX}"
else
    i=1
    while true; do
        current="${targetfolder}_v$((i++))"
        if [[ ! -e "$current" ]]; then
            targetfolder="$current"
            break
        fi
        echo "FILE $current EXISTS"
    done
fi

cd build/pic_build
cp "$config" ./etc/picongpu/config.cfg

i=0
suffix=""
while [[ -n "$1" ]]; do
    arg="$1"
    template="$(__f)"
    shift
    ./bin/tbg -c ./etc/picongpu/config.cfg \
        -t "$template" \
        "${targetfolder}${suffix}" \
        -s
    suffix=".$((i++))"
done
